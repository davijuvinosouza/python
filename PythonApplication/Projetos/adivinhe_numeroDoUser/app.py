import random


# Note: This method is an alias for randrange(start, stop+1).


def guess(x):
    random_number = random.randint(1, x)
    value = 0
    while value != random_number:
        value = int(input(f"Guess a number between 1 and {x}: "))
        if value < random_number:
            print("Sorry, guess again. Too low.")
        elif value > random_number:
            print("Sorry, guess again. Too high.")

    print(f"Yay, congrats. You have guessed the number {random_number}")


def computer_guess(x):
    low = 1
    high = x
    feedback = ''
    while feedback != 'c':
        if low != high:
            guess = random.randint(low, high)
        else:
            guess = low  # could also be high b/c low = high
        feedback = input(
            f'Is {guess} too high (H), too low (L), or correct (C)?? ').lower()
        if feedback == 'h':
            high = guess - 1
        elif feedback == 'l':
            low = guess + 1

        if high == 0:
            high = x

    print(f'Yay! The computer guessed your number, {guess}, correctly!')


computer_guess(10)
