import random


# Note: This method is an alias for randrange(start, stop+1).
def guess(x):
    random_number = random.randint(1, x)
    value = 0
    while value != random_number:
        value = int(input(f'Guess a number between 1 and {x}: '))
        if value < random_number:
            print('Sorry, guess again. Too low.')
        elif value > random_number:
            print('Sorry, guess again. Too high.')

    print(f'Yay, congrats. You have guessed the number {random_number}')


guess(10)
