# https://realpython.com/python-enumerate/

# Integração sem enumerate
values = ["a", "b", "c"]
for index in range(len(values)):
    value = values[index]
    print(index, value)


# exemplo simples 1
for count, value in enumerate(values):
    print(count, value)

# exemplo simples 2
colors = ["red", "green", "blue"]
for index, color in enumerate(colors):
    print(index, color)

# exemplo simples com inicio do index igual 1
for index, color in enumerate(colors, start=1):
    print(index, color)

#Conditional Statements to Skip Items
users = ["Test User", "Real User 1", "Real User 2"]
for index, user in enumerate(users):
    if index == 0:
        print("Extra verbose output for:", user)
    else:
        print(user)